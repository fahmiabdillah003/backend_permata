import { Sequelize } from "sequelize";

const db = new Sequelize("login_nodejs", "root", "", {
  host: "localhost",
  dialect: "mysql",
  define: {
    timestamps: false,
  },
});

export default db;
